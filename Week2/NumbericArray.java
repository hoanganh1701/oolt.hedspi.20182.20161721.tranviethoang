import java.util.Arrays;
import java.util.Scanner;

public class NumbericArray {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Nhap so phan tu cua mang: ");
        int n = Integer.parseInt(scan.nextLine());

        Integer A[] = new Integer[n];
        for (int i = 0; i < A.length; i++) {
            System.out.print("A[" + (i + 1) + "] : ");
            A[i] = Integer.parseInt(scan.nextLine());
        }

        Arrays.sort(A);

        System.out.print("\nSort: \n");
        System.out.print(Arrays.toString(A));

        double sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum = sum + A[i];
        }
        System.out.print("\nSUM = " + sum);

        double average = sum / n;
        System.out.print("\nAverage = " + average);

    }
}

