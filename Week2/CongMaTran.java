import java.util.Scanner;

public class CongMaTran {
    public static void main(String[] args) {
        int m, n, c, d;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap so hang va so cot: ");
        m = sc.nextInt();
        n = sc.nextInt();
        int A[][] = new int[m][n];
        int B[][] = new int[m][n];
        int C[][] = new int[m][n];
        System.out.println("Nhap ma tran A:");
        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                System.out.printf("A[%d][%d]= ", c, d);
                A[c][d] = sc.nextInt();
            }
        }
        System.out.println("Nhap ma tran B:");
        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                System.out.printf("B[%d][%d]= ", c, d);
                B[c][d] = sc.nextInt();
            }
        }

        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                C[c][d] = A[c][d] + B[c][d];
            }
        }
        System.out.println("Tong 2 ma tran: ");
        for (c = 0; c < m; c++) {
            for (d = 0; d < n; d++) {
                System.out.print(C[c][d] + "\t");
            }
            System.out.println();
        }
    }
}
