package Date;

import java.util.Scanner;

public class MyDate {
    private int day, month, year;

    public MyDate() {

    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        return "Ngay " + day + " thang " + month + " nam " + year;
    }

    public void accept() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ngay: ");
        day = scanner.nextInt();
        System.out.println("Thang: ");
        month = scanner.nextInt();
        System.out.println("Nam: ");
        year = scanner.nextInt();
    }

    public void print(MyDate myDate) {
        System.out.println(myDate);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
