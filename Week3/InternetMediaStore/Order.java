package InternetMediaStore;


public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    private int qtyOrdered;

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered > MAX_NUMBERS_ORDERED) {
            System.out.println("The order is almost full");
        }else {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
            System.out.println("So luong hang: " + qtyOrdered);
            System.out.println("The disc has been added ");
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        qtyOrdered--;
        System.out.println("So luong hang: "+qtyOrdered);
    }

    public float totalCost() {
        float sum = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            sum += itemsOrdered[i].getCost();
        }
        return sum;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

}
