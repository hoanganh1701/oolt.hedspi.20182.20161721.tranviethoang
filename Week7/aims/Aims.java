package aims;


import aims.media.MemoryDeamon;
import aims.order.Order;

import java.util.Scanner;

public class Aims {
    static void Menu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args) {
        int ch;
        MemoryDeamon memoryDeamon = new MemoryDeamon();
        Thread thread = new Thread(memoryDeamon);
        thread.setDaemon(true);
        thread.start();

        Order order = null;
        Scanner sc = new Scanner(System.in);
        Menu();
        while (true) {

            System.out.print("Chon: ");
            ch = sc.nextInt();
            sc.nextLine();
            switch (ch) {
                case 1:
                    System.out.println("Nhap id: ");
                    String id = sc.nextLine();
                    order = new Order(id);
                    break;
                case 2:
                    order.addItem();
                    break;
                case 3:
                    System.out.println("Nhap id: ");
                    String str = sc.nextLine();
                    order.removeMedia(str);
                    break;
                case 4:
                    order.print();
                    break;
                case 0:
                    return;
            }
        }
    }

}
