package aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private int length;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public CompactDisc(String id, String title) {
        super(id, title);
    }

    public CompactDisc(String id, String title, String category) {
        super(id, title, category);
    }

    public CompactDisc(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public CompactDisc(String id, String title, String category, String director, float cost) {
        super(id, title, category, director, cost);
    }

    public CompactDisc(String id, String title, String category, String director, int length, float cost) {
        super(id, title, category, director, length, cost);
    }
    public CompactDisc(String id, String title, String category, String director, int length, float cost,String artist) {
        super(id, title, category, director, length, cost);
        this.artist=artist;
    }


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void addTrack(Track track) {
        tracks.add(track);
    }

    public void removeTrack(Track track) {
        tracks.remove(track);
    }

    public int getLength() {
        return length;
    }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());

    }
}
