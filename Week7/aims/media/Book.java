package aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {

    private List<String> authors = new ArrayList<String>();

    public Book(String id, String title) {
        super(id, title);
    }

    public Book(String id, String title, String category) {
        super(id, title, category);
    }

    public Book(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public void addAuthor(String authorName) {
        authors.add(authorName);
        System.out.println("Da them " + authorName);
    }

    public void removeAuthor(String authorName) {
        authors.remove(authorName);
        System.out.println("Da xoa" + authorName);
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }
}
