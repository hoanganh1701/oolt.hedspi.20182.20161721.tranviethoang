package aims.media;

public class DigitalVideoDisc extends Disc implements Playable {

    private String director;
    private int length;

    public DigitalVideoDisc(String id, String title) {
        super(id, title);
    }

    public DigitalVideoDisc(String id, String title, String category) {
        super(id, title, category);
    }

    public DigitalVideoDisc(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public DigitalVideoDisc(String id, String title, String category, float cost, String director, int length) {
        this(id, title, category, cost);
        this.director = director;
        this.length = length;
    }

    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public boolean search(String tilte) {
        String[] str1 = this.getTitle().split(" ");
        String[] str2 = tilte.split(" ");
        boolean find = false;
        for (int i = 0; i < str2.length; i++) {
            for (int j = 0; j < str1.length; j++) {
                if (str2[i].equals(str1[j])) {
                    find = true;
                    break;
                }
            }
            if (find == false) {
                break;
            }
        }
        return find;
    }


    public String getDirectory() {
        return director;
    }

    public void setDirectory(String diretory) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

}
