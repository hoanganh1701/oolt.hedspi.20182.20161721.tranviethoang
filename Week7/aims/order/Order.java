package aims.order;

import aims.media.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Order {
    private String id;
    public static final int MAX_NUMBERS_ORDERED = 10;
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();

    public Order(String id) {
        this.id = id;
    }

    public void addMedia(Media media) {
        if (itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
            itemsOrdered.add(media);
        } else {
            System.out.println("Da them" + media.getTitle());
        }

    }

    public void removeMedia(String id) {
        for (Media media : itemsOrdered) {
            if (media.getId().equals(id)) {
                itemsOrdered.remove(media);
                System.out.println("Da xoa " + media.getId());
                return;
            }

        }
    }


    public float totalCost() {
        float sum = 0;
        for (Media media : itemsOrdered) {
            sum += media.getCost();
        }
        return sum;
    }

    public void print() {
        System.out.println("*********************Order************************");
        System.out.println("Ordered Items:");
        for (Media media : itemsOrdered) {
            media.printMedia();
        }
        System.out.println("Total cost: " + totalCost() + '$');
        System.out.println("****************************************\n");
    }

    public void addItem() {
        int c;
        System.out.println("1.DVD");
        System.out.println("2.Book");
        System.out.println("3.CD");
        System.out.print("Chon: ");
        Scanner sc = new Scanner(System.in);
        c = sc.nextInt();
        System.out.print("ID: ");
        String id = sc.nextLine();
        sc.nextLine();
        System.out.print("Title: ");
        String title = sc.nextLine();

        System.out.print("Category: ");
        String category = sc.nextLine();

        System.out.print("Cost: ");
        float cost = sc.nextFloat();
        if (c == 1) {
            System.out.print("Director: ");
            String director = sc.nextLine();
            sc.nextLine();
            System.out.print("Length: ");
            int length = sc.nextInt();
            sc.nextLine();
            DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, cost, director, length);
            addMedia(dvd);

        } else if (c == 2) {
            Book book = new Book(id, title, category, cost);
            System.out.print("Nhap so tac gia: ");
            int nbAuthors = sc.nextInt();
            sc.nextLine();

            for (int i = 0; i < nbAuthors; i++) {
                System.out.print("Ten tac gia: ");
                String author = sc.nextLine();
                book.addAuthor(author);
            }
            addMedia(book);
        }
        if (c == 3) {
            System.out.print("Ten tac gia: ");
            String author = sc.nextLine();
            System.out.print("Artist: ");
            String artist = sc.nextLine();
            sc.nextLine();
            System.out.print("Length: ");
            int length = sc.nextInt();
            sc.nextInt();
            CompactDisc compactDisc = CompactDisc(id, title, category, artist, length);
            compactDisc.addTrack(compactDisc);

        }

    }
}


