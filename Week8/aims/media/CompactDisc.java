package aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public CompactDisc(String id, String title) {
        super(id, title);
    }

    public CompactDisc(String id, String title, String category) {
        super(id, title, category);
    }

    public CompactDisc(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public CompactDisc(String id, String title, String category, String director, float cost) {
        super(id, title, category, director, cost);
    }

    public CompactDisc(String id, String title, String category, String director, int length, float cost) {
        super(id, title, category, director, length, cost);
    }

    public CompactDisc(String id, String title, String category, String director, int length, float cost, String artist) {
        super(id, title, category, director, length, cost);
        this.artist = artist;
    }


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void addTrack(Track track) {
        tracks.add(track);
    }

    public void removeTrack(Track track) {
        tracks.remove(track);
    }


    public void play() {
        System.out.println("Artist: " + this.getArtist());
        System.out.println("CD length: " + this.getLength());
        for (Track track : this.tracks) {
            track.play();
        }
    }

    public int getLength() {
        int length = 0;
        for (Track track : tracks) {
            length += track.getLength();
        }
        return length;
    }
    public int getTrackLength(){
        return this.tracks.size();
    }
    @Override
    public int compareTo(Media media){
        CompactDisc cd = (CompactDisc) media;

        int nbTrack1 = this.getTrackLength();
        int nbTrack2 = cd.getTrackLength();

        if (nbTrack1 < nbTrack2) return -1;
        if (nbTrack1 > nbTrack2) return 1;

        int length1 = this.getLength();
        int length2 = cd.getLength();

        if (length1 < length2) return -1;
        if (length1 > length2) return 1;
        return 0;
    }
}
