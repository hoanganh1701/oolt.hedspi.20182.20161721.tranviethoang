package aims.media;

import java.util.*;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();
    private String content;
    private List<String> contentTokens = new ArrayList<String>();
    private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();

    public Book(String id, String title) {
        super(id, title);
    }

    public Book(String id, String title, String category) {
        super(id, title, category);
    }

    public Book(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public Book(String id, String title, String category, float cost, String content) {
        super(id, title, category, cost);
        this.content = content;
        processContent();
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
        processContent();
    }

    public void addAuthor(String authorName) {
        authors.add(authorName);
        System.out.println("Da them " + authorName);
    }

    public void removeAuthor(String authorName) {
        authors.remove(authorName);
        System.out.println("Da xoa" + authorName);
    }

    public void processContent() {
        for (String string : content.split(" ")) {
            string = string.toLowerCase();
            contentTokens.add(string);
            boolean hasKey = wordFrequency.containsKey(string);
            if (!hasKey) {
                wordFrequency.put(string, 1);
            } else {
                wordFrequency.replace(string, wordFrequency.get(string) + 1);
            }
        }

        Collections.sort(contentTokens);
    }

    public void print() {
        print();
        System.out.println("Authors: " + this.authors.toString());
    }

    @Override
    public String toString() {
        for (Map.Entry m : wordFrequency.entrySet()) {
            System.out.println("\n" + m.getKey() + ": " + m.getValue());
        }
        return "Id: " + getId() + " Title: " + getTitle() + " Category: "
                + getCategory() + " Cost: "
                + getCost() + " Content length: " + contentTokens.size();
    }

}