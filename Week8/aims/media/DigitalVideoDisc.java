package aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
    public DigitalVideoDisc(String s, String title, String category, float cost, String id, int length){
        super(id);
    }
    public DigitalVideoDisc(String id, String title) {
        super(id, title);
    }

    public DigitalVideoDisc(String id, String title, String category) {
        super(id, title, category);
    }

    public DigitalVideoDisc(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    public DigitalVideoDisc(String id, String title, String category, String director, float cost) {
        super(id, title, category, director, cost);
    }

    public DigitalVideoDisc(String id, String title, String category, String director, int length, float cost) {
        super(id, title, category, director, length, cost);
    }
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    public boolean search(String tilte) {
        String[] str1 = this.getTitle().split(" ");
        String[] str2 = tilte.split(" ");
        boolean find = false;
        for (int i = 0; i < str2.length; i++) {
            for (int j = 0; j < str1.length; j++) {
                if (str2[i].equals(str1[j])) {
                    find = true;
                    break;
                }
            }
            if (find == false) {
                break;
            }
        }
        return find;
    }



    @Override
    public int compareTo(Media media) {

        DigitalVideoDisc dvd = (DigitalVideoDisc) media;
        float cost1 = this.getCost();
        float cost2 = dvd.getCost();

        if (cost1 < cost2) return -1;
        if (cost1 > cost2) return 1;
        return 0;
    }
}
