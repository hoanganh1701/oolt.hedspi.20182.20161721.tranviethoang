package aims.media;

public abstract class Media implements Comparable<Media> {
    private String id;
    private String title;
    private String category;
    private float cost;

    public Media(String id) {
        this.id = id;
    }

    public Media(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public Media(String id, String title, String category) {
        this(id, title);
        this.category = category;
    }

    public Media(String id, String title, String category, float cost) {
        super();
        this.id = id;
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void printMedia() {
        System.out.println("/" + id + "/" + title + "/" + category + "/" + cost);
    }

    public int compareTo(Media media) {
        if (media == this) {
            return 0;
        }
        return this.title.compareTo(media.title);
    }
}
