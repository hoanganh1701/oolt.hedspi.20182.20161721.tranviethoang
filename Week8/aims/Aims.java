package aims;


import aims.media.DigitalVideoDisc;
import aims.media.MemoryDeamon;
import aims.order.Order;

import java.util.*;

public class Aims {
    static void Menu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args) {

//        MemoryDeamon daemon = new MemoryDeamon();
//        Thread thread = new Thread(daemon);
//        thread.setDaemon(true);
//        thread.start();
//
//        int ch;
//        Scanner sc = new Scanner(System.in);
//        Order order = null;
//
//        do {
//            Menu();
//            System.out.println("Nhap: ");
//            ch = sc.nextInt();
//            sc.nextLine();
//            System.out.println();
//            switch (ch) {
//                case 1: {
//                    System.out.print("Nhap id: ");
//                    String id = sc.nextLine();
//                    order = new Order(id);
//                    break;
//                }
//
//                case 2: {
//                    order.addItem();
//                    break;
//                }
//
//                case 3: {
//                    System.out.print("Nhap id: ");
//                    String id = sc.nextLine();
//                    order.removeMedia(id);
//                    break;
//                }
//
//                case 4: {
//
//                    order.print();
//                    break;
//                }
//
//                default:
//                    if (ch != 0) {
//                        System.out.println("Thoat");
//                    }
//            }
//        } while (ch != 0);
//        sc.close();
        List<DigitalVideoDisc> dics = new ArrayList<DigitalVideoDisc>();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("1", "The lion king", "Animation", 19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("2", "Star Wars", "Science Fiction", 24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("3", "Aladdin", "Animation", 18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);

        dics.add(dvd1);
        dics.add(dvd2);
        dics.add(dvd3);

        System.out.println("-------------------------------------");
        System.out.println("The DVDs currently in the order are: ");
        for (DigitalVideoDisc disc : dics) {
            System.out.println(disc.getTitle());
        }
        Collections.sort(dics);
        System.out.println("-------------------------------------");
        System.out.println("The DVDs in sorted order are: ");
        for (DigitalVideoDisc disc : dics) {
            System.out.println(disc.getTitle());
        }

        System.out.println("-------------------------------------");
    }

}
