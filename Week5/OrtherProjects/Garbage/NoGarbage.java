package OrtherProjects.Garbage;

import java.io.FileReader;

public class NoGarbage {
    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader("D:test.txt");
        long start = System.currentTimeMillis();
        int i;
        StringBuilder sb = new StringBuilder();
        while ((i = fr.read()) != -1) {
            sb.append((char) i);
        }

        System.out.println(sb.toString());
        System.out.println(System.currentTimeMillis() - start);
    }
}
