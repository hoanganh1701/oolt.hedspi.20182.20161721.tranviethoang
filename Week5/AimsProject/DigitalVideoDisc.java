package AimsProject;

public class DigitalVideoDisc {
    private String tilte;
    private String category;
    private String diretory;
    private int length;
    private float cost;

    public DigitalVideoDisc() {
    }

    public DigitalVideoDisc(String tilte) {
        super();
        this.tilte = tilte;

    }

    public DigitalVideoDisc(String tilte, String category) {
        this.tilte = tilte;
        this.category = category;
    }

    public DigitalVideoDisc(String tilte, String category, String diretory) {
        this.tilte = tilte;
        this.category = category;
        this.diretory = diretory;
    }

    public DigitalVideoDisc(String tilte, String category, String directory, int length, float cost) {
        this.tilte = tilte;
        this.category = category;
        this.diretory = directory;
        this.length = length;
        this.cost = cost;
    }

    public boolean search(String tilte) {
        String[] str1 = this.tilte.split(" ");
        String[] str2 = tilte.split(" ");
        boolean find = false;
        for (int i = 0; i < str2.length; i++) {
            for (int j = 0; j < str1.length; j++) {
                if (str2[i].equals(str1[j])) {
                    find = true;
                    break;
                }
            }
            if (find == false) {
                break;
            }
        }
        return find;
    }

    public String getTilte() {
        return tilte;
    }

    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDiretory() {
        return diretory;
    }

    public void setDirectory(String diretory) {
        this.diretory = diretory;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
}
