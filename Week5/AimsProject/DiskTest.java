package AimsProject;

public class DiskTest {
    public static void main(String[] args) {
        Order anOrder = new Order();

        DigitalVideoDisc dvdl = new DigitalVideoDisc("The lion King");
        dvdl.setCategory("Animation");
        dvdl.setCost(19.95f);
        dvdl.setDirectory("Roger Allers");
        dvdl.setLength(87);
        anOrder.addDigitalVideoDisc(dvdl);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Scieence Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirectory("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirectory("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);

        DigitalVideoDisc dvd4 = new DigitalVideoDisc("alita", "animation", "abc", 15, 15.06f);
        anOrder.addDigitalVideoDisc(dvd4);

        anOrder.getALuckyItem();


    }
}
