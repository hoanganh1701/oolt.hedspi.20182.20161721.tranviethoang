package InternetMediaStore;


import java.util.Date;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 5;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    private int qtyOrdered = 0;
    private Date dateOrdered = java.util.Calendar.getInstance().getTime();
    private static int nbOrders = 0;
    public static int MAX_LIMITTED_ORDERS = 5;

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered < MAX_NUMBERS_ORDERED) {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
            System.out.println("So luong hang: " + qtyOrdered);
            System.out.println("The disc has been added ");
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void print() {
        System.out.println("*********************Order************************");
        System.out.println("Date: " + dateOrdered);
        System.out.println("Ordered Items:");
        for (int i = 0; i < qtyOrdered; i++) {
            System.out.println((i + 1) + ". DVD - " + itemsOrdered[i].getTilte() + " - " + itemsOrdered[i].getCategory()
                    + " - " + itemsOrdered[i].getDiretory() + " - "
                    + itemsOrdered[i].getLength() + ":" + itemsOrdered[i].getCost() + "$");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
        if (qtyOrdered < MAX_NUMBERS_ORDERED) {
            System.arraycopy(dvdList, 0, itemsOrdered, qtyOrdered, dvdList.length);
            qtyOrdered += dvdList.length;
            System.out.println("So luong hang: " + qtyOrdered);
            System.out.println("The disc has been added ");
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
        if (qtyOrdered < MAX_NUMBERS_ORDERED) {
            this.addDigitalVideoDisc(dvd1);
            this.addDigitalVideoDisc(dvd2);
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        qtyOrdered--;
        System.out.println("So luong hang: " + qtyOrdered);
    }

    public float totalCost() {
        float sum = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            sum += itemsOrdered[i].getCost();
        }
        return sum;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(Date dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

}
