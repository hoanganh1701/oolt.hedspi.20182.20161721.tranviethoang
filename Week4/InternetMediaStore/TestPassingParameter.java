package InternetMediaStore;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

        swap(jungleDVD, cinderellaDVD);
        System.out.println("jungle dvd title: " + jungleDVD.getTilte());
        System.out.println("cinderella dvd title: " + cinderellaDVD.getTilte());

        changTitle(jungleDVD,cinderellaDVD.getTilte());
        System.out.println("jungle dvd title: "+jungleDVD.getTilte());
    }

    public static void swap(Object o1, Object o2) {
        Object tmp = o1;
        o1 = o2;
        o2 = tmp;
    }

    public static void changTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTilte();
        dvd.setTilte(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
